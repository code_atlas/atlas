import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Fab from '@material-ui/core/Fab';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import NavigationIcon from '@material-ui/icons/Navigation';

import './Button.css';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  },
  margin: {
    margin: theme.spacing.unit,
  },
  extendedIcon: {
    marginRight: theme.spacing.unit,
  },
});


function ButtonCreate(props) {
  const { classes } = props;
  return (    
    <Button variant={props.variant} size={props.size} color={props.color} className={classes.margin}>
      <i className='material-icons button-create-icon-left button-create-icon'>{props.icon}</i> 
      {props.value}
    </Button>            
  );
}

ButtonCreate.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ButtonCreate);