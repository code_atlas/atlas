import React from 'react';

// react DnD
import { ItemTypes } from './Constants';
import { DragSource } from 'react-dnd';

import BasicCard from './BasicCard.js'

const cardSource = {
  beginDrag(props) {
    return {};
  }
};

function collect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  }
}

function DragableCard({ connectDragSource, isDragging }) {
  return connectDragSource(
    <div 
      style={{
        opacity: isDragging ? 0.5 : 1,
        fontSize: 25,
        fontWeight: 'bold',
        cursor: 'move'
      }}
      className="DnDDrag"
    >
      <BasicCard />
    </div>
  );
}

export default DragSource(ItemTypes.CARD, cardSource, collect)(DragableCard);