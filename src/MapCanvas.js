import React from 'react';



// react DnD
import { ItemTypes } from './Constants';
import { DropTarget } from 'react-dnd';


function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),    
  };
}

class MapCanvas extends React.Component {    
  render() {

    const {connectDropTarget} = this.props;

    return connectDropTarget(
        <div 
          style={{          
            width: '100%',
            height: '800px'
          }}
          className="DnDTarget"
        >          
        Target
        </div>      
      );
  }
}

export default DropTarget(ItemTypes.CARD, {}, collect)(MapCanvas);