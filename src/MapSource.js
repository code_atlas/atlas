import React from 'react';

import MapCanvas from './MapCanvas.js'

import DragableCard from './CardModal.js'


// react DnD
import { DragDropContextProvider } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

class MapSource extends React.Component {  

  render() {
    return (
      <DragDropContextProvider backend={HTML5Backend}>
        <div className="DnDWrap">
          <DragableCard />
          <MapCanvas />          
        </div>
      </DragDropContextProvider>
      );
  }
}

export default MapSource;