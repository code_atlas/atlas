import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import paper from 'paper';

// the followin  library is used to create routes for navigation inside the app
// read about it at https://reacttraining.com/react-router/web/example/basic
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

import MapSource from './MapSource.js';


////////////////////////////////////////////////////////////////////////////////

// Inbuilt
import Grid from '@material-ui/core/Grid';

// Custom
import Button from './Button.js';

import Card from './Card.js';

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

import ToDo from './toDo/index.js';
import Timer from './timer/index.js';
import Music from './music/index.js';

////////////////////////////////////////////////////////////////////////////////

// //this is entry point into the app
// class App extends React.Component {

//   constructor(props){
//     super(props);
//     this.state = {
//     }
//   }

//   render() {
//     return (
//       <div>
//       <Router>
//         <Switch>
//           <Route
//             exact path='/'
//             render={(props) => <MapView {...props} />}
//           />
//           <Route
//             path='/screen'
//             render={(props) => <ScreenView {...props} />}
//           />
//           <Route
//             path='/map'
//             render={(props) => <MapSource {...props} />}
//           />
//         </Switch>
//       </Router>
//       </div>
//     )
//   }
// }


// // This is one of the two views in the app
// class MapView extends React.Component {

//   constructor(props) {
//     super(props);
//     this.state = {
//       title: "Product Hunt",
//       mapData: {
//         screens: [
//           {
//             id: 1000,
//             title: "Homepage",
//             start: [50, 50],
//           },
//           {
//             id: 1001,
//             title: "Details",
//             start: [200, 300],
//           }
//         ],
//         connectors: [
//           {
//             from: 1000,
//             to: 1001 
//           }
//         ]
//       }
//     };
//   }


//   render() {
//     return (
//       <div>
//         <AppHeader 
//           title = {this.state.title}
//         ></AppHeader>
//         <Link to="/screen">Go to screen view</Link>
//         <Toolbox />
//         <Map 
//           mapData = {this.state.mapData}
//         />
//       </div>
//       );
//   }
// }


// // This is one of the two views in the app
// class ScreenView extends React.Component {

//   render() {
//     return(
//       <div>
//       <AppHeader></AppHeader>
//       <Link to="/">Go back to map view</Link>
//       <h1>Screen view</h1>
//       <DisplayScreen></DisplayScreen>
//       </div>
//       );
//   }  
// }

// class DisplayScreen extends React.Component{
//   render() {
//     return(

//       <div></div>

//       );
//   }
// }

// ////////////////////////////////////////////// Playground ///////////////////////////////////////////////////////////////

// // notes : paperjs operators (+,-,*,/) doesn't work with Point() and Size(), user math functions instead (Point().add(newPoint))

// class ScreenViewTestCanvas extends React.Component {

//   componentDidMount(){    
//     // defining scope of paperjs, limiting handler function to screen view so it doesn't interfier with handlers in other pages
//     let screenScope = new paper.PaperScope(); 
//     let canvas = document.getElementById('testCanvas');
//     screenScope.setup(canvas);
//     ////////////////////////////////////////////////////////////

//     let points = 25;

//     let length = 35;

//     let path = new paper.Path({
//       strokeColor: '#E4141B',
//       strokeWidth: 20,
//       strokeCap: 'round'
//     });

//     let start = screenScope.view.center.divide([10,1]);        
//     for (let i = 0; i < points; i++)      
//       path.add(start.add(new paper.Point(i * length, 0)));

//   }  

//   render() {
//     return(
//       <div>        
//        <canvas id="testCanvas" resize="true" ></canvas>
//       </div>
//       );
//   }
// }
// ////////////////////////////////////////////// Playground ///////////////////////////////////////////////////////////////


// // This is the floating menu in the MapView
// class Toolbox extends React.Component {  

//   render() {
//     return (
//       <div>
//         <button>Create Screen</button>
//         <button>Create Connection</button>  
//       </div>
//       );
//   }  
// }




// // This is the canvas view containing the map in the MapView
// class Map extends React.Component {

//   constructor(props) {
//     super(props);
//     this.handleClick = this.handleClick.bind(this);
//   }





//   componentDidMount(){

//     // defining scope of paperjs, limiting handler function to map view so it doesn't interfier with handlers in other pages
//     let mapScope = new paper.PaperScope(); 
//     let canvas = document.getElementById('myCanvas');
//     mapScope.setup(canvas);
//     //////////////////////////////////////////////////////////////////

//     this.renderCanvas(this.props.mapData, mapScope);
//     let screenTool = new paper.Tool();

//     screenTool.onMouseDown = event => {
//       this.handleClick(event, mapScope);
//     }
    
//   }




//   renderCanvas(mapData, mapScope){

//     // Iterate over the mapData to draw all the screens
//     for (let i=0; i<mapData.screens.length; i++) {
//       this.renderScreen(mapData.screens[i], mapScope);
//     }

//     // Iterate over the mapData object to draw every connection
//     for (let i=0; i<mapData.connectors.length; i++) {
//       console.log(mapData.connectors[i].from);
//       let start = mapData.connectors[i].from;
//       let end = mapData.connectors[i].to;
//       let startScreen = mapData.screens.find(x => x.id === start);
//       let endScreen = mapData.screens.find(x => x.id === end);
//       this.renderConnection(startScreen, endScreen);
//     }
//   }


//   // Given a screen, this function draws it on the convas
//   renderScreen(screen, mapScope){
//     let x = screen.start[0];
//     let y = screen.start[1];
//     let startPoint = new paper.Point(x,y);
//     let endPoint = new paper.Point(x+100, y+100);
//     let rectangle = new paper.Rectangle(startPoint, endPoint);
//     let path = new paper.Path.Rectangle(rectangle);
//     path.fillColor = '#fff';
//     mapScope.view.draw();
//   }


//   // Given two screen objects, this function draw a connector between them
//   renderConnection(startScreen, endScreen){
//     let x1 = startScreen.start[0]+50;
//     let y1 = startScreen.start[1];
//     let x2 = endScreen.start[0]+50;
//     let y2 = endScreen.start[1];
//     let startPoint = new paper.Point(x1,y1);
//     let endPoint = new paper.Point(x2,y2);
//     let myPath = new paper.Path();
//     myPath.strokeColor = 'black';
//     myPath.add(startPoint, endPoint);
//   }

//   // fillPattern(canvas){
//   //   let context = canvas.getContext('2d');
//   //   let img = new Image();
//   //   img.src = './pattern.png';
//   //   img.onload = function() {
//   //   context.fillStyle = context.createPattern(this,"repeat");
//   //   context.fillRect(0, 0, canvas.width, canvas.height);
//   //   };
//   // }


//   //this function will handle all clicks inside the canvas
//   handleClick(event,mapScope) {
//     console.log(event.screenX);
//     let x = event.point.x;
//     let y = event.point.y;
//     let screen = {
//             id: 1003,
//             title: "Homepage",
//             start: [x, y],
//           }
//     this.renderScreen(screen, mapScope);
//   }
  

//   render() {
//     return (
//       <div>        
//        <canvas id="myCanvas" resize="true" ></canvas>
//       </div>
//     );
//   }
// }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 16th April
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////


//this is entry point into the app
class Atlas extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      title: "Your projects",
      apiResponse:"",
    }
  }

  // integrating data base
  // callAPI() {
  //     fetch("http://localhost:9000/testBackend")
  //         .then(res => res.text())
  //         .then(res => this.setState({ apiResponse: res }));
  // }

  // componentWillMount() {
  //     this.callAPI();
  // }

  // render() {
  //   return (
  //     <div>
  //       <AppHeader></AppHeader>

  //       <div className="container-main">
  //         <Grid container spacing={24}>
  //           <Grid item xs={10}>            
  //             <h4>{this.state.title}</h4>
  //             <h4>{this.state.apiResponse}</h4>
  //           </Grid>
  //           <Grid item xs={2}>
  //             <Button 
  //               size="small" 
  //               color="primary" 
  //               variant="contained" 
  //               icon="add" 
  //               value="New project"
  //             ></Button>
  //           </Grid>
  //         </Grid>          

  //         <Card
  //           title="First project" 
  //           desc="This is a short description of the first project"
  //           screens="23" 
  //           flows="11"
  //         ></Card>

  //         <Music>          
  //         </Music>

  //         <Timer>
  //         </Timer>

  //         <ToDo>          
  //         </ToDo>          

  //       </div>          
  //     </div>
  //   )
  // }

  render() {
    return (
      <div>        
        <Music>
        </Music>

        <Timer>
        </Timer>

        <ToDo>
        </ToDo>
      </div>
    )
  }
}

//This is the header to be rendered on top of each page
class AppHeader extends React.Component {
  render() {
    return (
      <div id="header">
        <img id="logo" src="./logo.png" alt="logo"/>        
      </div>
      )
  }
}

ReactDOM.render(
  <Atlas />,
  document.getElementById('root')
);