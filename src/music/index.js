import React, {useState, useEffect} from 'react';
import soundFile from './audioFiles/London Grammar - Hey Now  (Official Video).mp3'
import soundFile2 from './audioFiles/07 Runnin.mp3'

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Fab from '@material-ui/core/Fab';
import CloudQueue from '@material-ui/icons/CloudQueue';
import FilterHdr from '@material-ui/icons/FilterHdr';
import PlayArrow from '@material-ui/icons/PlayArrow';
import Pause from '@material-ui/icons/Pause';

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
  fab: {
    margin: theme.spacing.unit,
  },
  margin: {
    margin: theme.spacing.unit,
  },
  active:{
    backgroundColor:'rgba(0, 0, 0, 0.08)',
  },
  inactive:{

  }
});

const Music = (props) => {

  const { classes } = props;

  const allAudioFiles = {
    'audio1': new Audio(soundFile),
    'audio2' : new Audio(soundFile2)
  }

  Object.keys(allAudioFiles).forEach((key)=>{
    allAudioFiles[key].loop = true;
  });
  // 
  const [selectedMusic, setSelectedMusic] = useState({});    
  const [test, setTest] = useState(true);

  const audioHandler = (event,name) => {    
    // IMPORTANT use this method to copy a json else reach gives problems
    const newSelected = {...selectedMusic};
    if (newSelected[name]) {
      newSelected[name].pause();
      delete newSelected[name];
    } else {
      newSelected[name] = allAudioFiles[name];
      newSelected[name].play();
    }
    setSelectedMusic(newSelected);
    // setTest(!test);
    console.log(selectedMusic['audio1'] ? 'active':'inactive')    
  }

  return (
    <div>
      <Paper className={classes.root} elevation={1}>        
        <Typography component="p" align='center'>          
          <IconButton aria-label="" className={`${classes.margin} ${selectedMusic['audio1'] ? classes.active:classes.inactive}`} onClick={event=>audioHandler(event,'audio1')}>
            <CloudQueue fontSize="large"/>            
          </IconButton>
          <IconButton aria-label="" className={`${classes.margin} ${selectedMusic['audio2'] ? classes.active:classes.inactive}`} onClick={event=>audioHandler(event,'audio2')}>
            <FilterHdr fontSize="large"/>
          </IconButton>          
        </Typography>        
      </Paper>        
    </div>    
  );
};

Music.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Music);