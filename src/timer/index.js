import React, {useState, useEffect} from 'react';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Fab from '@material-ui/core/Fab';
import Replay from '@material-ui/icons/Replay';
import PlayArrow from '@material-ui/icons/PlayArrow';
import Pause from '@material-ui/icons/Pause';

import TimerSettings from './timerSettings/index'

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
  fab: {
    margin: theme.spacing.unit,
  },
});

function Timer(props) {
  const { classes } = props;

  // time
  const [baseTime, setBaseTime] = useState(70);  

  const [time, setTime] = useState(baseTime);  

  const [active, setActive] = useState(false);  

  const minutes = Math.floor(time/60);
  const seconds = time-(minutes*60);

  const minutesString = (minutes<10) ? '0'+ minutes : minutes;
  const secondsString = (seconds<10) ? '0'+ seconds : seconds;

  useEffect(
    () => {
      let interval;
      if (active) {
        interval = setInterval(tick, 1000);
      }
      // return a function to be run when the array values are changed 
      return () => clearInterval(interval);
    },
    [active]
  );

  const tick = () => {
    // hooks and setInterval doesn't work together, hence :
    setTime(time=>time-1);
  }   

  const handleStartStop = event => {    
    setActive(active?false:true);
  }    

  const handleReset = event => {   
    setActive(false);
    setTime(baseTime);
  }  

  const formSubmit = event => {
    event.preventDefault();
    // const todo = {'text':value, 'status':false};
    // if (value != '') {
    //   setTodos([...todos,todo]);  
    // }    
    // setValue('');    
  }

  const onChangeHandler = event => {
    // setValue(event.target.value);
  }  

  const handleApply = newTime => {    
    setTime(newTime);
    setBaseTime(newTime);
  }

  return (
    <div>
      <Paper className={classes.root} elevation={1}>
        <TimerSettings time={time} handleApply={handleApply}/>
        <Typography component="h2" variant="display4" align='center' gutterBottom>
          {minutesString} : {secondsString}
        </Typography>
        <Typography component="p" align='center'>
          <Fab color="primary" aria-label="Add" className={classes.fab} onClick={handleReset}>
            <Replay />
          </Fab>
          <Fab color="primary" aria-label="Add" className={classes.fab} onClick={handleStartStop}>
            {active?(<Pause />):(<PlayArrow />)}
          </Fab>          
        </Typography>        
      </Paper>
    </div>
  );
}

Timer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Timer);