import React, {useState} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import Settings from '@material-ui/icons/Settings';

const styles = theme => ({  
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },  
  button: {
    margin: theme.spacing.unit,
  },
});

const TimerSettings = (props) => {

  const { classes } = props;

  const [open,SetOpen] = useState(false);
  const [newTime, setNewTime] = useState(props.time);  

  const handleClickOpen = () => {
    SetOpen(true);
  };

  const handleClose = () => {
    SetOpen(false);
  };  

  const onChangeHandler = event => {
    const re = /^[0-9\b]+$/;
    if (event.target.value === '' || re.test(event.target.value)) {        
      setNewTime((event.target.value)*60)
    }    
  }  

  const handleClick = event => {    
    props.handleApply(newTime);
    handleClose();
  }

  return (
    <div>      
      <IconButton className={classes.button} aria-label="Settings" onClick={handleClickOpen}>
        <Settings />        
      </IconButton>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Settings</DialogTitle>
        <DialogContent>          
          <TextField
            id="outlined-name"
            label="Work interval (minutes)"
            className={classes.textField}                        
            margin="normal"
            variant="outlined"
            value={Math.floor(newTime/60)}
            onChange={onChangeHandler}
          />          
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleClick} color="primary">
            Apply
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

TimerSettings.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TimerSettings);