import React, {useState} from 'react';
import ToDoForm from './toDoForm/index.js';
import CheckboxList from './toDoList/index.js'

const ToDo = () => {

  // json to save the value entered and status, if checked or not
  const [value, setValue] = useState('');

  // an array of all the values
  const [todos, setTodos] = useState([]);

  // form submit
  const createHandler = event => {
    event.preventDefault();
    const todo = {'text':value, 'status':false};
    if (value != '') {
      setTodos([...todos,todo]);  
    }    
    setValue('');    
  }

  const onChangeHandler = event => {
    setValue(event.target.value);
  }

  const deleteHandler = (event,index) => {    
    // never change the state manually, always create a new const and then use setState()
    const newTodos = [...todos];
    newTodos.splice(index, 1);    
    setTodos(newTodos);    
  }

  const updateHandler = (event,index) => {    
    // never change the state manually, always create a new const and then use setState()
    const newTodos = [...todos];
    newTodos[index]['status'] = newTodos[index]['status'] ? false:true;        
    setTodos(newTodos);    
  }

  return (
    <div>
      <ToDoForm 
        submitHandler={createHandler} 
        onChangeHandler={onChangeHandler} 
        value={value}>
      </ToDoForm>

      <CheckboxList 
        updateHandler={updateHandler} 
        deleteHandler={deleteHandler} 
        todos={todos}>
      </CheckboxList>
    </div>    
  );
};

export default ToDo;