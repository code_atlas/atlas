import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
  container: {    
    flexWrap: 'wrap',
    textAlign:'center'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width:'60%'
  },
  dense: {
    marginTop: 16,
  },
  menu: {
    width: 200,
  },
});

const ToDoForm = (props) => {

  const { classes } = props;

  return (
    <form className={classes.container} onSubmit={props.submitHandler}>
        <TextField
          id="outlined-name"
          label="Task"
          className={classes.textField}          
          onChange={props.onChangeHandler}
          margin="normal"
          variant="outlined"
          value={props.value}        
        />
    </form>    
  );
}

ToDoForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ToDoForm);