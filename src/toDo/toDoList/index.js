import React, {useState} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

const styles = theme => ({
  root: {    
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
  listItemShow: {
  },
  listItemHide: {    
  }
});

const ToDoList = (props) => {

  const { classes } = props;

  return (
    <div>    
      <Paper className={classes.root} elevation={1}>
        <List className={classes.root}>
          {props.todos.map((value,index) => (
            <ListItem 
              key={index} 
              role={undefined}            
              dense 
              button 
              onClick={
                (event)=>{props.updateHandler(event,index)}
              }
            >
              <Checkbox
                checked={value.status}
                tabIndex={-1}
                disableRipple
              />
              <ListItemText primary={value.text} />
              <ListItemSecondaryAction>
                <IconButton 
                  aria-label="Delete" 
                  onClick={
                    (event)=>{props.deleteHandler(event,index)}
                  }
                >
                  <DeleteIcon />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          ))}
        </List>
      </Paper>
    </div>
  );
}

ToDoList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ToDoList);